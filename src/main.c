#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "mem.h"
#include "util.h"
#include "mem_internals.h"

static void* heap;
static struct block_header* heap_start;

static struct block_header *get_block_header(void *data) {
    return (struct block_header*) ((uint8_t*) data - offsetof(struct block_header, contents));
}

/*
    Allocation test
*/

void test1() {
    printf("Starting Test-1:\n");
    void* mem = _malloc(2000);
    if (mem == NULL) err ("malloc failed to allocate the memory\n");
    if (heap_start -> capacity.bytes != 2000) err ("Wrong size of allocated memory\n");
    debug_heap(stderr, heap);
    printf("Test-1 was successful:\n");
    _free(mem);
    debug_heap(stderr, heap);
}

/*
    Freeing test
*/

void test2() {
    printf("Starting Test-2:\n");
    void* first = _malloc(1000);
    void* second = _malloc(1000);
    if (first == NULL || second == NULL) err ("malloc failed to allocate the memory. Heap state:\n");
    else printf ("Memory was allocated successfully. Heap state:\n");
    debug_heap(stderr, heap);
    _free(first);
    _free(second);
    if(!heap_start -> is_free) err ("Failed to free allocated memory\n");
    printf ("Memory was freed successfully\n");
    debug_heap(stderr, heap);
    printf("Test-2 was successful:\n");
}

/*
    Heap extension test
*/

void test3() {
    printf("Starting Test-3:\n");
    void* mem = _malloc(10000*2);
    struct block_header *block = get_block_header(mem);
    if (mem == NULL) err ("malloc failed to allocate the memory\n");
    if (block -> capacity.bytes != 20000) err ("Wrong size of allocated memory\n");
    debug_heap(stderr, heap);
    printf("Test-3 was successful:\n");
    _free(mem);
    debug_heap(stderr, heap);
}

void test4() {
    printf("Starting Test-4:\n");
    void* first = _malloc(3000);
    debug_heap(stderr, heap);
    void* second = _malloc(7000);
    debug_heap(stderr, heap);
    _free(second);
    _free(first);
    debug_heap(stderr, heap);
    void* third = _malloc(10000);
    debug_heap(stderr, heap);
    _free(third);
    debug_heap(stderr, heap);
    printf("Test-4 was successful:\n");
}

int main() {
    heap = heap_init(10000);
    heap_start = (struct block_header*) (heap);
    test1();
    test2();
    test3();
    test4();
    return 0;
}